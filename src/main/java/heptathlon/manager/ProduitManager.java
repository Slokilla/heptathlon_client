package heptathlon.manager;

import heptathlon.entity.Famille;
import heptathlon.entity.Magasin_Produits;
import heptathlon.entity.Produit;

import java.util.List;

public class ProduitManager extends TableOperations<Produit> {

    private static ProduitManager instance = null;

    public static synchronized ProduitManager getInstance() {
        if (instance == null)
            instance = new ProduitManager();
        return instance;
    }

    private ProduitManager() {
        super();
    }

    @Override
    public Produit create(Produit p) {
        trx.begin();
        session.save(p);
        trx.commit();

        return p;
    }

    @Override
    public void delete(Produit p) {
        em.remove(p);
        em.flush();
        em.clear();
    }

    @Override
    public Produit update(Produit p) {
        trx.begin();
        session.update(p);
        trx.commit();

        return p;
    }

    @Override
    public Produit findById(final int id) {
        return session.createQuery("select p from Produit p where p.id = :id", Produit.class)
                .setParameter("id", id)
                .getSingleResult();
    }

    @Override
    public List<Produit> findAll() {
        return session.createQuery("from Produit ", Produit.class)
                .getResultList();
    }

    public List<Produit> searchByName(final String name) {
        return session.createQuery(
                "SELECT p " +
                        "FROM Produit p " +
                        "WHERE lower(p.reference) " +
                        "LIKE lower(:name) ", Produit.class)
                .setParameter("name", name +"%")
                .getResultList();
    }

    @Override
    public Produit findByName(final String name) {
        return session.createQuery(
                "SELECT p " +
                        "FROM Produit p " +
                        "WHERE p.reference = : name ", Produit.class)
                .setParameter("name", name)
                .getSingleResult();
    }

    /**
     * Consulter le stock d'un article dans un magasin.
     *
     * @param id l'identifiant du produit'
     * @return la quantité en stock du produit specifié
     */
    public int countById(final int id){
        Magasin_Produits stock = session.createQuery("" +
                "select p " +
                "from Produit p " +
                "where p.id = :id_prod ", Magasin_Produits.class)
                .setParameter("id_prod", id)
                .getSingleResult();

        return stock.getQuantite();
    }


    /**
     * Récupérer la liste des articles d'une famille de produits.
     *
     * @param id l'id de la famille d'articles
     * @return une liste des articles correspondants a la famille
     */
    public List<Produit> findByFamille(final int id){
        return session.createQuery("from Produit " +
                        "         where famille.id = :id", Produit.class)
                                  .setParameter("id", id)
                                  .getResultList();
    }

    /**
     * Récupérer la liste des articles d'une famille de produits.
     *
     * @param f la famille d'articles
     * @return une liste des articles correspondants a la famille
     */
    public List<Produit> findByFamille(final Famille f){
        return session.createQuery("from Produit " +
                        "         where famille.id = :id", Produit.class)
                                  .setParameter("id", f.getId())
                                  .getResultList();
    }

    /**
     * Récupérer la liste des articles d'une famille de produits
     * à partir d'une recherche sur le nom.
     *
     * @param s la chaine à chercher
     * @return une liste des articles correspondants au résultat de la recherche
     */
    public List<Produit> findByFamille(final String s){
        return session.createQuery("from Produit " +
                        "         where lower(famille.nom) LIKE lower(:name)", Produit.class)
                                  .setParameter("name", s+"%")
                                  .getResultList();
    }

    /**
     * Méthode qui retourne le stock d'un produit
     * @param p le produit dont on consulte le stock
     * @return le stock
     */
    public Magasin_Produits findStock(final Produit p){
        return session.createQuery("" +
                "select mp " +
                "from Magasin_Produits mp " +
                "where mp.produitsId = :id_prod ", Magasin_Produits.class)
                .setParameter("id_prod", p.getId())
                .getSingleResult();
    }

    /**
     * Méthode qui retourne le stock d'un produit
     * @param id l'id du produit dont on consult le stock
     * @return le stock
     */
    public Magasin_Produits findStock(final int id){
        return session.createQuery("" +
                "from Magasin_Produits " +
                "where produitsId = :id_prod ", Magasin_Produits.class)
                .setParameter("id_prod", id)
                .getSingleResult();
    }

    public void saveStock(final Magasin_Produits mp){
        session.beginTransaction();
        session.saveOrUpdate(mp);
        session.getTransaction().commit();
    };
}

