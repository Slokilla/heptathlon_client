package heptathlon;

import heptathlon.operations.CentralClient;
import heptathlon.operations.CentralService;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.StringWriter;

public class Client {
    public static void main(String[] args) throws JAXBException {
        CentralService central = CentralClient.getInstance().getClient();

        StringWriter sw = new StringWriter();

        Changements c = Changements.getInstance();

        JAXBContext ctx = JAXBContext.newInstance(Changements.class);
        Marshaller m = ctx.createMarshaller();

        m.marshal(c, sw);

        central.envoyerFactures(sw.toString());


    }
}
