package heptathlon;

import heptathlon.operations.OperationService;
import heptathlon.operations.Operations;

import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public final class Main {
    public static void main(String[] args) throws RemoteException {
        System.out.println("Lancement du serveur");

        try {
            // Create an instance of the server object
            Operations ope = Operations.getInstance();
            System.out.println("Démarrage du magasin Heptathlon " + Env.getInstance().getNumMagasin() + "...");
            Registry result = LocateRegistry.createRegistry(1099);
            Naming.rebind(OperationService.LOOKUPNAME, ope);

            System.out.println("Server is Listening");


        } catch (Exception e) {
            System.err.println("Oula ça pue");
            System.out.println(e.getMessage());
            System.exit(1);
        }

    }
}
