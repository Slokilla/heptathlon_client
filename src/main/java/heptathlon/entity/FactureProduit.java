package heptathlon.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Facture_Produit", schema = "decathlon_loc_1")
public class FactureProduit {
    private int id;

    @Id
    @Column(name = "ID")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FactureProduit that = (FactureProduit) o;

        return id == that.id;
    }

    @Override
    public int hashCode() {
        return id;
    }
}
