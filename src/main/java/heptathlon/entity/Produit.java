package heptathlon.entity;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import java.util.Objects;

@Entity
@Table(name = "Produit", schema = "decathlon_loc_1")
@XmlRootElement
@XmlSeeAlso(Famille.class)
public class Produit {
    private int id;
    private String reference;
    private Double prix;
    private Famille famille;



    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "Reference")
    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    @Basic
    @Column(name = "Prix")
    public Double getPrix() {
        return prix;
    }

    public void setPrix(Double prix) {
        this.prix = prix;
    }


    /**
     * Gestion de la relation Produit --> Famille
     */
    @ManyToOne
    @JoinColumn(name="famille_ID",
                foreignKey = @ForeignKey(name = "Produit_Famille_ID_fk")
    )
    public Famille getFamille() {
        return famille;
    }

    public void setFamille(Famille famille) {
        this.famille = famille;
    }


    ///////CONSTRUCTORS////////


    public Produit(String reference, Double prix) {
        this.reference = reference;
        this.prix = prix;
    }

    public Produit() {
    }


    ///////SURCHARGES/////////

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Produit that = (Produit) o;

        if (id != that.id) return false;
        if (!Objects.equals(reference, that.reference)) return false;
        return Objects.equals(prix, that.prix);
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (reference != null ? reference.hashCode() : 0);
        result = 31 * result + (prix != null ? prix.hashCode() : 0);
        return result;
    }
}
