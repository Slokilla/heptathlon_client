package heptathlon.entity;

import heptathlon.Env;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "Magasin_Produits", schema = "decathlon_loc_1")
public class Magasin_Produits {
    private int id;
    private Integer magasinId;
    private Integer produitsId;
    private Integer quantite;

    @Id
    @Column(name = "ID")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "magasin_ID")
    public Integer getMagasinId() {
        return magasinId;
    }

    public void setMagasinId(Integer magasinId) {

    }

    @Basic
    @Column(name = "produit_ID")
    public Integer getProduitsId() {
        return produitsId;
    }

    public void setProduitsId(Integer produitsId) {
        this.produitsId = produitsId;
    }

    @Basic
    @Column(name = "quantite")
    public Integer getQuantite() {
        return quantite;
    }

    public void setQuantite(Integer quantite) {
        this.quantite = quantite;
    }

    public void addOne() {
        this.quantite++;
    }

    public void removeOne() {
        this.quantite--;
    }

    public void addMany(int quantity) {
        this.quantite += quantity;
    }


    public Magasin_Produits() {
        Env prop = Env.getInstance();
        magasinId = prop.getNumMagasin();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Magasin_Produits that = (Magasin_Produits) o;

        if (id != that.id) return false;
        if (!Objects.equals(magasinId, that.magasinId)) return false;
        if (!Objects.equals(produitsId, that.produitsId)) return false;
        return Objects.equals(quantite, that.quantite);
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (magasinId != null ? magasinId.hashCode() : 0);
        result = 31 * result + (produitsId != null ? produitsId.hashCode() : 0);
        result = 31 * result + (quantite != null ? quantite.hashCode() : 0);
        return result;
    }



}
