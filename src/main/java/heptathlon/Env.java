package heptathlon;

import java.io.*;
import java.util.Properties;

public class Env {

    private static Env instance = null;
    private int numMagasin;
    private String dbName;

    private Env() {
        try{
            InputStream input = new FileInputStream("src/main/resources/environment.properties");
            Properties prop = new Properties();
            prop.load(input);

            numMagasin = Integer.parseInt(prop.getProperty("numMagasin"));
            dbName = prop.getProperty("dbName");


        } catch (IOException io) {
            io.printStackTrace();
        }
    }

    public int getNumMagasin() {
        return numMagasin;
    }

    public String getDbName() {
        return dbName;
    }

    public static synchronized Env getInstance() {
        if (instance == null)
            instance = new Env();
        return instance;
    }

    private void loadData(){

    }

}
