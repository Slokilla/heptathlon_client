package heptathlon.wrapper;
import heptathlon.entity.Famille;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import java.util.List;

@XmlRootElement()
@XmlSeeAlso(Famille.class)
public class FamilleList {

    @XmlElement(name = "Famille")
    private List<Famille> list;

    public FamilleList(){}

    public FamilleList(List<Famille> list){
        this.list=list;
    }

    public List<Famille> getList(){
        return list;
    }
}

