package heptathlon.wrapper;
import heptathlon.entity.Famille;
import heptathlon.entity.Produit;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import java.util.List;

@XmlRootElement()
@XmlSeeAlso(Produit.class)
public class ProduitList {

    @XmlElement(name = "Produit")
    private List<Produit> list;

    public ProduitList(){}

    public ProduitList(List<Produit> list){
        this.list=list;
    }

    public List<Produit> getList(){
        return list;
    }
}

