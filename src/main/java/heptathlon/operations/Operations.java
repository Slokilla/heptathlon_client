package heptathlon.operations;

import heptathlon.Changements;
import heptathlon.Exceptions.BadDateFormatException;
import heptathlon.Exceptions.OutOfStockException;
import heptathlon.entity.*;
import heptathlon.factory.sessionFactory;
import heptathlon.manager.FactureManager;
import heptathlon.manager.FamilleManager;
import heptathlon.manager.ProduitManager;
import heptathlon.wrapper.FamilleList;
import heptathlon.wrapper.ProduitList;
import org.hibernate.Metamodel;
import org.hibernate.Session;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.StringReader;
import java.io.StringWriter;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.List;

/**
 * Utilisation : Operation.getInstance(). ---> Merci je suis pas triso ¯\_(ツ)_/¯
 */
public final class Operations extends UnicastRemoteObject implements OperationService{

    /**
     * Session permettant l'acces a la base.
     */
    Session session;

    FamilleManager familleManager;
    ProduitManager produitManager;
    FactureManager factureManager;

    final Metamodel metamodel;

    /**
     * instance permettant de créer un singleton.
     */
    private static volatile Operations instance = null;

    /**
     * Constructeur de l'objet.
     */
    private Operations() throws RemoteException {
        super();
        session = sessionFactory.getSession();
        metamodel = session.getSessionFactory().getMetamodel();
        factureManager = FactureManager.getInstance();
        familleManager = FamilleManager.getInstance();
        produitManager = ProduitManager.getInstance();
    }

    /**
     * Méthode permettant de renvoyer une instance de la classe Operations.
     *
     * @return Retourne l'instance du singleton.
     */
    public static synchronized Operations getInstance() throws RemoteException {
        if (Operations.instance == null) {
            Operations.instance = new Operations();
        }
        return Operations.instance;
    }

    //TODO fonction pour Passer une commande interne entre magasins


    @Override
    public void acheterUnProduit(int id) throws OutOfStockException, RemoteException {
        System.out.println("Produit en cours d'acquisition...");
        Magasin_Produits stock = produitManager.findStock(id);

        if (stock.getQuantite() == 0){
            throw new OutOfStockException();
        }

        stock.removeOne();

        produitManager.saveStock(stock);

        //return produitManager.findById(id);
    }

    @Override
    public String editerFacture(String produits, String modePaiement) throws RemoteException, JAXBException {
        System.out.println("Edition de la facture");

        //R2CUPERATION DE LA FACTURE
        JAXBContext jaxbContext = JAXBContext.newInstance(ProduitList.class);
        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

        ProduitList lf = (ProduitList) jaxbUnmarshaller.unmarshal(new StringReader(produits));

        ArrayList<Produit> p = new ArrayList<>(lf.getList());
        ArrayList<Produit> pPlein = new ArrayList<>();
        for(Produit pr : p ){
            pPlein.add(produitManager.findByName(pr.getReference()));
        }

        Facture f = new Facture(pPlein, modePaiement);

        //GESTION DES PRODUITS
        for (Produit toBuy : f.getProduits()) {
            try {
                acheterUnProduit(toBuy.getId());
            }catch (OutOfStockException e){
                System.err.println(e.getMessage());
            }
        }

        //Persist de la facture et ajout à la liste de nouvelles
        factureManager.create(f);
        //Changements.getInstance().add(f);


        //RETOUR DE LA FACTURE
        StringWriter sw = new StringWriter();
        System.out.println(f.getId());
        jaxbContext = JAXBContext.newInstance(Facture.class);
        Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
        //Required formatting??
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        jaxbMarshaller.marshal(f, sw);

        return sw.toString();
    }

    @Override
    public void annulerFacture(Facture f) throws RemoteException{
        for (Produit p : f.getProduits()){
            Magasin_Produits mp = produitManager.findStock(p.getId());
            mp.addOne();
        }
    }

    @Override
    public String getFacture(final int id) throws RemoteException {
        Facture f = factureManager.findById(id);
        return generateFactureBody(f);
    }

    @Override
    public String getFacture(final Facture f) throws RemoteException {
        return generateFactureBody(f);
    }

    @Override
    public float getTurnover(final String date) throws BadDateFormatException, RemoteException {

        System.out.println("Calcul du chiffre d'affaire pour le " + date);

        if (!date.matches("^\\d{4}-\\d{2}-\\d{2}$")){
            throw new BadDateFormatException("Le format de la date ne correspond pas à AAAA-MM-DD");
        }

        float total = 0;

        for (Facture facture : factureManager.findByDate(date)) {
            total += facture.getTotal();
        }

        System.out.println("Calcul terminé, envoie au client");
        return total;
    }


    @Override
    public void addProduct(final int idProduit,
                           final int quantity) throws RemoteException {
        System.out.println("Ajout d'un produit dans la base de donnée...");

        Magasin_Produits stock = produitManager.findStock(idProduit);
        stock.addMany(quantity);
        produitManager.saveStock(stock);

        System.out.println("Le produit a été ajouté !");
    }


    /**
     * Procédure générant le corps d'une facture
     * @param f un objet facture
     * @return la facture mise en forme
     */
    private String generateFactureBody(Facture f) {
        StringBuilder texteFacture = new StringBuilder();
        texteFacture.append("Facture numéro ").append(f.getId()).append("\n");
        texteFacture.append("Date : ").append(f.getDateFacture()).append("\n");

        for (Produit p : f.getProduits()) {
            texteFacture.append(p.getReference()).append("   ---> ").append(p.getPrix()).append("\n");
        }

        texteFacture.append("Total : ").append(f.getTotal()).append("€");

        return texteFacture.toString();
    }

    @Override
    public String getProduct(final int id) throws RemoteException {
        System.out.println("Récupération du produit "+ Integer.toString(id));
        Produit p = produitManager.findById(id);
        StringWriter sw = new StringWriter();

        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(Produit.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
            //Required formatting??
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

            jaxbMarshaller.marshal(p, sw);
        } catch (JAXBException e) {
            e.printStackTrace();
        }

        return sw.toString();

    }

    @Override
    public String getAllFamille() throws RemoteException{
        System.out.println("Récupération des familles");
        List<Famille> familles = familleManager.findAll();
        ArrayList<String> fs = new ArrayList<>();

        StringWriter sw = new StringWriter();
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(FamilleList.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
            //Required formatting??
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

            FamilleList familleList = new FamilleList(familles);

            jaxbMarshaller.marshal(familleList, sw);
        } catch (JAXBException e) {
            e.printStackTrace();
        }

        return sw.toString();
    }

    @Override
    public String getAllProduit() throws RemoteException{
        System.out.println("Récupération des Produits");
        List<Produit> produits = produitManager.findAll();
        ArrayList<Produit> fs = new ArrayList<>();

        StringWriter sw = new StringWriter();

        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(ProduitList.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
            //Required formatting??
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

            ProduitList produitList = new ProduitList(produits);

            jaxbMarshaller.marshal(produitList, sw);
        } catch (JAXBException e) {
            e.printStackTrace();
        }

        return sw.toString();
    }

}
