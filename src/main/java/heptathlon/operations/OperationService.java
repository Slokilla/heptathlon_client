package heptathlon.operations;

import heptathlon.Exceptions.BadDateFormatException;
import heptathlon.Exceptions.OutOfStockException;
import heptathlon.entity.Facture;

import javax.xml.bind.JAXBException;
import java.rmi.Remote;
import java.rmi.RemoteException;

public interface OperationService extends Remote {
    String LOOKUPNAME = "DecatServer";

    //TODO fonction pour Passer une commande interne entre magasins


    /**
     * Methode qui enlève 1 à la quantité d'un produit, et qui le retourne pour ajouter à une facture
     *
     * @param id l'id d'un produit
     * @throws OutOfStockException si il n'y a plus de produit en stock
     */
    void acheterUnProduit(int id) throws OutOfStockException, RemoteException;

    /**
     * Créer un objet facture complet (pas persisté)
     *
     * @param produits     les produits à ajouter à la facture
     * @param modePaiement le mode de paiement du client
     * @return La facture crée
     */
    String editerFacture(String produits, String modePaiement) throws RemoteException, JAXBException;

    /**
     * Annule une facture en remettant ses produits en stock
     *
     * @param f la facture à annuler
     */
    void annulerFacture(Facture f) throws RemoteException;

    /**
     * Retourne une facture au format texte.
     *
     * @param id le numero de la facture a consulter
     * @return la facture au format texte
     */
    String getFacture(final int id) throws RemoteException;

    /**
     * Consulter une facture au format texte.
     *
     * @param f la facture a consulter
     * @return la facture au format texte
     */
    String getFacture(final Facture f) throws RemoteException;

    /**
     * Calcul le chiffre d'affaire à une date donnée.
     *
     * @param date la date sur laquelle faire le calcul
     * @return le chiffre d'affaire de cette date (turnover).
     * @throws BadDateFormatException si la date n'est pas bien formatée
     */
    float getTurnover(final String date) throws BadDateFormatException, RemoteException;

    /**
     * Recharge le stock d'un produit dans un magasin.
     *
     * @param idProduit le produit concerné
     * @param quantity  la quantité a ajouter
     */
    void addProduct(final int idProduit,
                    final int quantity) throws RemoteException;

    String getProduct(final int id) throws RemoteException;

    String getAllFamille() throws RemoteException;

    String getAllProduit() throws RemoteException;
}
