package heptathlon.operations;

import java.rmi.Remote;

public interface CentralService extends Remote {


    String LOOKUPNAME = "ServerCentral";

    void envoyerFactures(String factures);

    /**
     * Recharge le stock d'un produit dans un magasin.
     *
     * @param idProduit le produit concerné
     * @param quantity  la quantité a ajouter
     */
    void addProduct(final int idProduit,
                    final int quantity);

    String getProduct(final int id);
}
