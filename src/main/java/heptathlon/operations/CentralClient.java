package heptathlon.operations;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

public class CentralClient {

    private static CentralClient instance = null;
    CentralService client;

    private CentralClient() {
        try {
            client = (CentralService) Naming.lookup(CentralService.LOOKUPNAME);
        } catch (NotBoundException | RemoteException | MalformedURLException e) {
            e.printStackTrace();
        }
    }

    public static synchronized CentralClient getInstance(){
        if (instance == null)
            return new CentralClient();
        return instance;
    }

    public CentralService getClient() {
        return client;
    }
}
