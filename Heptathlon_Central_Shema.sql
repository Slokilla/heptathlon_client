create table Facture
(
    ID           int auto_increment,
    ModePaiement varchar(255) null,
    DateFacture  varchar(255) not null,
    magasin_ID   int          not null,
    total        float        null,
    designation  varchar(255) not null,
    constraint Facture_ID_uindex
        unique (ID)
);

alter table Facture
    add primary key (ID);

create table Famille
(
    ID  int auto_increment,
    nom varchar(255) null,
    constraint Famille_ID_uindex
        unique (ID)
);

alter table Famille
    add primary key (ID);

create table Produit
(
    ID         int auto_increment,
    Reference  varchar(255) not null,
    Prix       float        not null,
    famille_ID int          null,
    constraint Produit_ID_uindex
        unique (ID),
    constraint Produit_Reference_uindex
        unique (Reference),
    constraint Produit_Famille_ID_fk
        foreign key (famille_ID) references Famille (ID)
);

alter table Produit
    add primary key (ID);

create table Facture_Produit
(
    ID          int auto_increment,
    facture_ID  int null,
    produits_ID int null,
    constraint Facture_Produits_ID_uindex
        unique (ID),
    constraint Facture_Produits_Facture_ID_fk
        foreign key (facture_ID) references Facture (ID),
    constraint Facture_Produits_Produit_ID_fk
        foreign key (produits_ID) references Produit (ID)
);

alter table Facture_Produit
    add primary key (ID);

create table Magasin_Produits
(
    ID         int auto_increment,
    magasin_ID int not null,
    produit_ID int not null,
    quantite   int null,
    constraint Magasin_Produits_ID_uindex
        unique (ID),
    constraint Magasin_Produits_Produit_ID_fk
        foreign key (produit_ID) references Produit (ID)
);

alter table Magasin_Produits
    add primary key (ID);


